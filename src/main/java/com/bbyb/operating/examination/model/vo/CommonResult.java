package com.bbyb.operating.examination.model.vo;

/**
 * 通用返回实体类
 * className: CommonResult
 * datetime: 2023/2/10 14:30
 * author: lx
 */
public class CommonResult<T> {

    /** 返回编码（0 成功 非0异常） */
    private int code;

    /** 返回结果信息描述 */
    private String msg;

    /** 是否成功 boolean表示 */
    private boolean success;

    /** 返回的数据 */
    private T data;

    public CommonResult(){
        this.code = 0;
        this.msg = "成功";
        this.success = true;
    }

    public CommonResult(T data){
        this();
        this.data = data;
    }

    public CommonResult(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public CommonResult(int code, String msg, boolean success, T data){
        this.code = code;
        this.msg = msg;
        this.success = success;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
