package com.bbyb.operating.examination.model.vo;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author ChengWei
 * @version 1.0
 * @data 2023/8/10 14:33
 * @fileName InsertUserRoleVo
 * @description: TODO
 */
@Data
public class InsertUserRoleVo {

    private Integer id;

    private String userCode;

    private String userName;

    private String phone;

    private String password;

    private String email;

    private String idCard;

    private Integer gender;

    private Integer isAdmin;

    private Integer isEnable;

    private String createBy;

    private LocalDateTime createTime;

    private String updateBy;

    private LocalDateTime updateTime;

    private Integer invalid;

    private List<Integer> roleIds;
}
