package com.bbyb.operating.examination.model.vo;

import lombok.Data;

/**
 * @author ChengWei
 * @version 1.0
 * @data 2023/8/10 17:05
 * @fileName UpdateByRoleVo
 * @description: TODO
 */
@Data
public class UpdateByRoleVo {
    private Integer roleId;
    private String roleCode;
}
