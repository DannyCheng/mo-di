package com.bbyb.operating.examination.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@ApiModel("Demo响应")
@Data
@AllArgsConstructor
public class DemoDO {
    @ApiModelProperty("str字符串")
    private String str;
}
