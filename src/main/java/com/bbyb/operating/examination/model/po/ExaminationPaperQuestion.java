package com.bbyb.operating.examination.model.po;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ExaminationPaperQuestion {
    private Integer id;

    private Integer paperId;

    private String questionTitle;

    private String quertionNum;

    private String quertionType;

    private Integer preQuestionId;

    private BigDecimal score;

    private String createBy;

    private LocalDateTime createTime;

    private String updateBy;

    private LocalDateTime updateTime;

    private Integer invalid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPaperId() {
        return paperId;
    }

    public void setPaperId(Integer paperId) {
        this.paperId = paperId;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public String getQuertionNum() {
        return quertionNum;
    }

    public void setQuertionNum(String quertionNum) {
        this.quertionNum = quertionNum;
    }

    public String getQuertionType() {
        return quertionType;
    }

    public void setQuertionType(String quertionType) {
        this.quertionType = quertionType;
    }

    public Integer getPreQuestionId() {
        return preQuestionId;
    }

    public void setPreQuestionId(Integer preQuestionId) {
        this.preQuestionId = preQuestionId;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getInvalid() {
        return invalid;
    }

    public void setInvalid(Integer invalid) {
        this.invalid = invalid;
    }
}