package com.bbyb.operating.examination.model.response;

import com.bbyb.operating.examination.model.po.RoleUser;
import lombok.Data;

/**
 * @author ChengWei
 * @version 1.0
 * @data 2023/8/10 19:08
 * @fileName OverResponse
 * @description: TODO
 */
@Data
public class OverResponse extends RoleUser {
    private String userCode;
    private String userName;
    private String roleCode;
    private String roleName;
}
