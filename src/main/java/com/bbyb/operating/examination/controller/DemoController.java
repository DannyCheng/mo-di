package com.bbyb.operating.examination.controller;

import com.bbyb.operating.examination.model.vo.DemoDO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Demo")
@RestController
@RequestMapping("/demo")
public class DemoController {

    @ApiOperation(value = "demo-one")
    @GetMapping("/one")
    public String demoOne(){
        return "Hello World!";
    }

    @ApiOperation(value = "demo-two")
    @PostMapping("/two")
    public DemoDO demoTwo(DemoDO demo){
        return demo;
    }
}
