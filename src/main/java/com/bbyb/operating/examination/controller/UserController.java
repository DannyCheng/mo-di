package com.bbyb.operating.examination.controller;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.response.OverResponse;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.model.vo.InsertUserRoleVo;
import com.bbyb.operating.examination.model.vo.UpdateByRoleVo;
import com.bbyb.operating.examination.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 账号
 * className: UserController
 * datetime: 2023/2/10 14:28
 * author: lx
 */
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private HttpServletRequest request;

    @GetMapping("/roleList")
    public List<Role> roleList(){
        log.info("功能名称:角色列表,请求URI:[{}],请求方式:[{}]",request.getRequestURI(),request.getMethod());
        List<Role> list = userService.roleList();
        log.info("功能名称:角色列表,请求URI:[{}],请求方式:[{}],响应结果:[{}]",request.getRequestURI(),request.getMethod(),list);
        return list;
    }

/*
    @PostMapping("/insertUserRole")
    public CommonResult insertUserRole(@RequestBody InsertUserRoleVo insertUserRoleVo){
        log.info("功能名称:添加角色,请求URI:[{}],请求方式:[{}],请求参数:[{}]",request.getRequestURI(),request.getMethod(),insertUserRoleVo);
        userService.insertUserToRole(insertUserRoleVo);
        return new CommonResult();
    }
*/

    /*
    * 角色的基础新增（角色编码名称必填，并且角色编码名称不能重复
    * */
    @PostMapping("/insertRole/{roleCode}")
    public void insertRole(@PathVariable String roleCode){
        userService.insertRole(roleCode);
    }

    /*
    * 删除角色
    * */
    @PostMapping("/deleteByRoleId/{roleId}")
    public void deleteByRoleId(@PathVariable Integer roleId){
        userService.deleteByRole(roleId);
    }

    /*
    * 修改角色
    * */
    @PostMapping("/updateByRole")
    public void updateByRole(@RequestBody UpdateByRoleVo updateByRoleVo){
        userService.updateByRole(updateByRoleVo);
    }

    /*
    * 根据id查询角色
    * */
    @PostMapping("/findByRole/{roleId}")
    public Role findByRole(@PathVariable Integer roleId){
        Role byRole = userService.findByRole(roleId);
        return byRole;
    }

    /*
     * 角色关联新增
     * */
    @PostMapping("/insertUserRole") //1111111111111111111111111111  9999999999999999999999999999999
    public void insertUserRole(@RequestBody InsertUserRoleVo insertUserRoleVo){
        userService.insertUserRole(insertUserRoleVo);
    }

    /*
    * 权限查询
    * */
    @PostMapping("/selectOver/{userId}") //dsadsadasaaaaaaaaaaaaaa
    public List<OverResponse> selectOver(@PathVariable Integer userId){
        List<OverResponse> list = userService.selectOver(userId);
        return list;
    }
}



