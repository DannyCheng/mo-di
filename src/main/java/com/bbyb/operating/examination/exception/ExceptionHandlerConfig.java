package com.bbyb.operating.examination.exception;

import com.bbyb.operating.examination.model.po.Result;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.HibernateValidator;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.context.annotation.Bean;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.stream.Collectors;

/**
 * @author ChengWei
 * @version 1.0
 * @data 2023/8/10 19:42
 * @fileName ExceptionHandlerConfig
 * @description: TODO
 */
@Slf4j
public class ExceptionHandlerConfig {

    /**
     * 全局处理异常
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    public Result handlerException(Exception e){
        e.printStackTrace();
        log.error(e.getMessage());
        return new Result().error(e.getMessage());
    }

    /**
     * 处理 form data方式调用接口校验失败抛出的异常
     * @param e
     * @return
     */
    @ExceptionHandler(BindException.class)
    public Result handlerException(BindException e){
        BindingResult bindingResult = e.getBindingResult();
        String result = bindingResult.getFieldErrors().stream().map(o -> o.getDefaultMessage()).collect(Collectors.joining(","));
        return new Result().error(result);
    }

    /**
     * 处理 json 请求体调用接口校验失败抛出的异常
     * @param e
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result handlerException(MethodArgumentNotValidException e){
        BindingResult bindingResult = e.getBindingResult();
        String result = bindingResult.getFieldErrors().stream().map(o -> o.getDefaultMessage()).collect(Collectors.joining(","));
        return new Result().error(result);
    }

    /**
     * 处理requestParam/PathVariable参数校验的异常
     * @param e
     * @return
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public Result handlerException(ConstraintViolationException e){
        String result = e.getConstraintViolations().stream().map(o -> o.getMessage()).collect(Collectors.joining(","));
        return new Result().error(result);
    }

    /**
     * spring Validation默认校验完所有参数才会返回结果，可将failFast设置为true,一旦校验失败就会结果校验并返回结果
     * @return
     */
    @Bean
    public Validator validator(){
        ValidatorFactory factory=Validation.byProvider(HibernateValidator.class).
                configure().failFast(true).buildValidatorFactory();
        return factory.getValidator();
    }
}
