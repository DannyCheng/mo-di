package com.bbyb.operating.examination.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bbyb.operating.examination.model.po.Role;

/**
 * @author ChengWei
 * @version 1.0
 * @data 2023/8/10 14:20
 * @fileName RoleService
 * @description: TODO
 */
public interface RoleService extends IService<Role> {
}
