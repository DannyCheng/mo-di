package com.bbyb.operating.examination.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.response.OverResponse;
import com.bbyb.operating.examination.model.vo.InsertUserRoleVo;
import com.bbyb.operating.examination.model.vo.UpdateByRoleVo;

import java.util.List;

public interface UserService extends IService<User> {


    List<Role> roleList();

/*
    public void insertUserToRole(InsertUserRoleVo insertUserRoleVo);
*/

    /*
     * 添加角色
     * */
    public void insertRole(String code);

    /*
     * 删除
     * */
    public void deleteByRole(Integer roleId);

    public void updateByRole(UpdateByRoleVo updateByRoleVo);

    /*
     * 查询角色
     * */
    public Role findByRole(Integer roleId);

    /*
     * 角色关联新增
     * */
    public void insertUserRole(InsertUserRoleVo insertUserRoleVo);

    List<OverResponse> selectOver(Integer userId);
}
