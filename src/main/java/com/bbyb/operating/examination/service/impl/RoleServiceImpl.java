package com.bbyb.operating.examination.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bbyb.operating.examination.mapper.RoleMapper;
import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ChengWei
 * @version 1.0
 * @data 2023/8/10 14:20
 * @fileName RoleServiceImpl
 * @description: TODO
 */
@Service  //创建tag分支然后上传一手
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

}
