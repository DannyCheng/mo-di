package com.bbyb.operating.examination.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bbyb.operating.examination.exception.BizException;
import com.bbyb.operating.examination.mapper.UserMapper;
import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.RoleUser;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.response.OverResponse;
import com.bbyb.operating.examination.model.vo.InsertUserRoleVo;
import com.bbyb.operating.examination.model.vo.UpdateByRoleVo;
import com.bbyb.operating.examination.service.RoleService;
import com.bbyb.operating.examination.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 账号服务
 * className: UserServiceImpl
 * datetime: 2023/2/10 14:29
 * author: lx
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper,User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RoleService roleService;

    @Override
    public List<Role> roleList() {
        List<Role> list = roleService.list();
        return list;
    }

    /*
    * 添加角色
    * */
    public void insertRole(String code){
        //查询role表里是否有这个编号名称
        Role byrole = userMapper.selectByRole(code);
        //如果存在   角色编码名称不能重复
        if(byrole!=null){
            throw new BizException(500,"此编号名称已存在");
        }else{
            //  角色编码名称必填
            if(byrole.getRoleCode()==null){
                throw new BizException(500,"角色编号名称必须填写");
            }else{
                //不存在继续添加
                Role role1 = new Role();
                role1.setRoleCode(code);
                userMapper.insertByRole(role1);
            }
        }
    }


    /*
    * 删除
    * */
    public void deleteByRole(Integer roleId){
        //查询中间表中与角色id关联的表
        List<RoleUser> users = userMapper.selectByUserRoleWithRoleId(roleId);
        //删除中间表
        for (RoleUser user : users) {
            userMapper.deleteByUserRoleWithRole(user.getId());
        }

        //删除role表
        Integer i = userMapper.deleteByRole(roleId);
        if(i<=0){
            throw new BizException(500,"删除失败");
        }
    }

    /*
    * 修改角色信息
    * */
    public void updateByRole(UpdateByRoleVo updateByRoleVo){
        //修改role中的信息
        Integer i = userMapper.updateByRoleCode(updateByRoleVo);
/*        if(i>0){
            //修改中间表中的信息

            //根据roleId查出中间表
            List<RoleUser> users = userMapper.selectByUserRoleWithRoleId(updateByRoleVo.getRoleId());
            //修改中间表中的信息
            if(null!=users){
                for (RoleUser user : users) {
                    userMapper.updateByUserRoleCode(updateByRoleVo);
                }
            }
        }*/
    }

    /*
    * 查询角色
    * */
    public Role findByRole(Integer roleId){
        Role role = roleService.getOne(new QueryWrapper<Role>().lambda().eq(Role::getId, roleId));
        return role;
    }

/*    *//*
     * 给用户添加角色
     * *//*
    public void insertUserToRole(InsertUserRoleVo insertUserRoleVo){

        Role role = insertUserRoleVo.getRole();
        User user = insertUserRoleVo.getUser();

        //添加用户
        userMapper.insertUser(insertUserRoleVo.getUser());

        //查询中间表
        List<RoleUser> roleUser = userMapper.selectByUserRole(user.getId());
        if(null!=roleUser){
            for (RoleUser userRole : roleUser) {
                Integer roleId = userRole.getRoleId();
                if(roleId==role.getId()){
                    //重复了
                    throw new BizException(500,"不好意思,角色重复");
                }
            }
            //添加中间表
            userMapper.insertUserRole(user.getId(),role.getId());
        }
    }*/

    /*
    * 角色关联新增
    * */
    public void insertUserRole(InsertUserRoleVo insertUserRoleVo){
        //添加用户
        userMapper.insertUser(insertUserRoleVo);
        //添加角色
        List<Integer> roleIds = insertUserRoleVo.getRoleIds();
        RoleUser roleUser = new RoleUser();
        roleUser.setUserId(insertUserRoleVo.getId());
        for (Integer roleId : roleIds) {
            roleUser.setRoleId(roleId);
            //添加中间表
            userMapper.insertUserRoleList(roleUser);
        }
    }

    @Override
    public List<OverResponse> selectOver(Integer userId) {
        return userMapper.selectOver(userId);
    }
}
