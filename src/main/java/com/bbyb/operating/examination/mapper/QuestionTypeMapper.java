package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.QuestionType;
import java.util.List;

public interface QuestionTypeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(QuestionType row);

    QuestionType selectByPrimaryKey(Integer id);

    List<QuestionType> selectAll();

    int updateByPrimaryKey(QuestionType row);
}