package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.ExaminationPlan;
import java.util.List;

public interface ExaminationPlanMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ExaminationPlan row);

    ExaminationPlan selectByPrimaryKey(Integer id);

    List<ExaminationPlan> selectAll();

    int updateByPrimaryKey(ExaminationPlan row);
}