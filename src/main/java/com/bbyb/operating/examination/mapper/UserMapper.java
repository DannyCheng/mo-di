package com.bbyb.operating.examination.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.RoleUser;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.response.OverResponse;
import com.bbyb.operating.examination.model.vo.InsertUserRoleVo;
import com.bbyb.operating.examination.model.vo.UpdateByRoleVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {

    Role selectByRole(String roleCode);



    List<RoleUser> selectByUserRole(Integer id);

    void insertUserRole(Integer id, Integer id1);

    void insertByRole(Role role1);

    List<RoleUser> selectByUserRoleWithRoleId(Integer roleId);

    void deleteByUserRoleWithRole(Integer id);

    Integer deleteByRole(Integer roleId);

    Integer updateByRoleCode(UpdateByRoleVo updateByRoleVo);

    void insertUserRoleList(RoleUser roleUser);

    void insertUser(InsertUserRoleVo insertUserRoleVo);

    List<OverResponse> selectOver(Integer userId);
}
