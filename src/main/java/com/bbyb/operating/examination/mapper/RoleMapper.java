package com.bbyb.operating.examination.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bbyb.operating.examination.model.po.Role;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleMapper extends BaseMapper<Role> {

}
